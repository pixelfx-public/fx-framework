# FX-Framework Issue Tracking

Issue Tracking for FX-Framework based products:

- N64Digital

- DCDigital

- PS1Digital

Before submitting an issue, please check [Issues](https://gitlab.com/pixelfx-public/fx-framework/-/issues) to see, if the issue was already submitted.

## Documentation / Guides

- [`docs.pixelfx.co`](https://docs.pixelfx.co)

## Firmware / Firmware Changelogs

- [`firmware.pixelfx.co`](https://firmware.pixelfx.co)

