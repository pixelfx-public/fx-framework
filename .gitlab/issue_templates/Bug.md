## Quick information
<!-- This is to help replicate the issue as closeley as possible, ignore anything irrelevant to your issue !-->
- **Mod variant (N64Digital/PS1Digital/DCDigital/...):**
- **Firmware version:**
- **Output Resolution:**
- **Region (NTSC/PAL/etc):**
- **Game:**
- **Flash card / ODE used:** 
- **Controller used:** 
- **TV / Monitor used:** 

## Observed issue
<!-- A brief description of the issue !-->

## Expected result
<!-- What is the expected behaviour !-->

## Steps to reproduce
<!-- List the steps required to produce the error. These should be as few as possible !-->

## Screenshots
<!-- Any relevant screenshots which show the issue !-->
